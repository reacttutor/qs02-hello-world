# Hello World

> [Quick Start: Hello World](https://reactjs.org/docs/hello-world.html)



    create-react-app hello-world --scripts-version=react-scripts-ts
    cd hello-world


Remove all "App" files under `/src` and update the `index.tsx`:

    ReactDOM.render(
      <h1>Hello, world!</h1>,
      document.getElementById('root') as HTMLElement
    );


Then, run a dev server:

    npm start




