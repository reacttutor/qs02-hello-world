import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.css';

ReactDOM.render(
  <h1>Hello, world!</h1>,
  document.getElementById('root') as HTMLElement
);
